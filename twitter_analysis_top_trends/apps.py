from django.apps import AppConfig


class TwitterAnalysisTopTrendsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'twitter_analysis_top_trends'
