import tweepy as tw
import pandas as pd
import requests
import re
import datetime


def get_twitter_api_access(API_keys: dict) -> tw.api:
    '''
    Connect to Twitter API and return it
    args:
        API_keys : dict of tokens
    returns:
        tw.api
    '''

    # Authenticate to Twitter API using Tweepy
    auth = tw.OAuthHandler(API_keys["API key"].get(0), API_keys["API secret key"].get(0))
    auth.set_access_token(API_keys["access token"].get(0), API_keys["access token secret"].get(0))

    return tw.API(auth, wait_on_rate_limit=True)


def get_tweet(API_keys: dict, tweet_id: int) -> requests.models.Response:
    '''
    Asks to Twitter API for a specific tweet and returns it.

    args:
        API_keys : dict of ID tokens
        tweet_id : int represents ID of the tweet
    returns
        requests.models.Response
    '''
    headers = {'Authorization': f"Bearer {API_keys['bearer Token'].get(0)}", }

    return requests.get(f'https://api.twitter.com/2/tweets/{tweet_id}', headers=headers)


def get_hashtag(text: str) -> list:
    '''
    Search for "#" in a text and returns all # and their text.

    args:
        text : str
    returns:
        list
    '''

    return re.findall(r'#\w+', text)


def push_hashtags(hashtags: pd.DataFrame, project_API: str = "http://127.0.0.1:8000/hashtag/"):
    for idx, row in hashtags.iterrows():
        data = {
            "hashtag": row['hashtag'],
            "date": row['date']
        }

        r = requests.post(project_API, data=data)

        if r.status_code != 201:
            print(r)


def get_random_tweets(API_keys: dict, api: tw.api,
                      since: datetime.date = pd.Timestamp.now().date() - pd.Timedelta(1, unit='d'),
                      until: datetime.date = pd.Timestamp.now().date(), min_retweets: int = 0, lang: str = "en",
                      n_returns: int = 5, query: str = '') -> list:
    """
    Queries Twitter API to get some random tweet.

    args:
        API_keys : dict of ID tokens
        api : tw.api
        since : datetime.date represent a date at this format : YYYY-MM-DD
        until : datetime.date represent a date at this format : YYYY-MM-DD
        min_retweets : int, minimum retweet of the tweet (to get most important ones)
        lang : str="en", represents the tweet language
        show : bool=False, set at True if you want to print tweets
        n_returns : int=5, quantity of returned tweets
    returns:
        list of tweets information
    """
    # Build query
    search_words = f"{query} min_retweets:{min_retweets} -filter:replies "
    if since:
        since = pd.Timestamp(since).date()
        search_words += f'since:{since} '
    if until:
        until = pd.Timestamp(until).date()
        search_words += f'until:{until} '
    if lang:
        search_words += f'lang:{lang}'

    # Collect tweets
    tweets = tw.Cursor(api.search, q=search_words).items(n_returns)
    print('Tweepy cursor created : ' + search_words)

    # Init dict
    hashtags_dict = {'hashtag': [], 'date': []}

    # Iterate and save and print tweets
    for tweet in tweets:
        # If tweet text is truncated, go search it
        if tweet.truncated:
            # If except is triggered, maybe an authentication error
            try:
                res = get_tweet(API_keys, tweet.id)
                content = res.json().get('data').get('text')
            except:
                # If except is triggered, maybe a connection error or too much demands
                try:
                    print(f'{res.status} {res.title} : "{res.detail}"')
                except:
                    print(res)
                    return hashtags_dict

                content = tweet.text
        else:
            content = tweet.text

        hash_list = get_hashtag(content)

        hashtags_dict['hashtag'] += hash_list
        hashtags_dict['date'] += [tweet.created_at] * len(hash_list)

    push_hashtags(pd.DataFrame(hashtags_dict))

    return hashtags_dict


def push_tweets(tweets: pd.DataFrame, project_API: str = "http://127.0.0.1:8000/tweet/"):
    for idx, row in tweets.iterrows():
        data = {
            'author_id': row['author_id'],
            'author_name': row['author_name'],
            'author_screen_name': row['author_screen_name'],
            'author_location': row['author_location'],
            'author_followers_count': row['author_followers_count'],
            'author_friends_count': row['author_friends_count'],
            'author_created_at': row['author_created_at'],
            'tweet_id': row['tweet_id'],
            'tweet_created_at': row['tweet_created_at'],
            'tweet_favorite_count': row['tweet_favorite_count'],
            'tweet_retweet_count': row['tweet_retweet_count'],
            'tweet_text': row['tweet_text']
        }

        r = requests.post(project_API, data=data)

        if r.status_code != 201:
            print(r)


def get_warcraft_tweets(API_keys: dict, api: tw.api,
                        since: datetime.date = pd.Timestamp.now().date() - pd.Timedelta(1, unit='d'),
                        until: datetime.date = pd.Timestamp.now().date(), min_retweets: int = 0, lang: str = "en",
                        n_returns: int = 5, query: str = '') -> list:
    """
    Queries Twitter API to get some random tweet.

    args:
        API_keys : dict of ID tokens
        api : tw.api
        since : datetime.date represent a date at this format : YYYY-MM-DD
        until : datetime.date represent a date at this format : YYYY-MM-DD
        min_retweets : int, minimum retweet of the tweet (to get most important ones)
        lang : str="en", represents the tweet language
        show : bool=False, set at True if you want to print tweets
        n_returns : int=5, quantity of returned tweets
    returns:
        list of tweets information
    """
    # Build query
    search_words = f"{query} min_retweets:{min_retweets} -filter:replies "

    if since:
        since = pd.Timestamp(since).date()
        search_words += f'since:{since} '
    if until:
        until = pd.Timestamp(until).date()
        search_words += f'until:{until} '
    if lang:
        search_words += f'lang:{lang}'

    # Collect tweets
    tweets = tw.Cursor(api.search, q=search_words).items(n_returns)
    print('Tweepy cursor created : ' + search_words)

    # Init dict
    tweet_list = []

    # Iterate and save and print tweets
    for tweet in tweets:
        # If tweet text is truncated, go search it
        if tweet.truncated:
            # If except is triggered, maybe an authentication error
            try:
                res = get_tweet(API_keys, tweet.id)
                content = res.json().get('data').get('text')
            except:
                # If except is triggered, maybe a connection error or too much demands
                try:
                    print(f'{res.status} {res.title} : "{res.detail}"')
                except:
                    print(res)
                    return tweet_list

                content = tweet.text
        else:
            content = tweet.text

        tweet_list.append({
            'author_id': tweet.author.id,
            'author_name': tweet.author.name,
            'author_screen_name': tweet.author.screen_name,
            'author_location': tweet.author.location,
            'author_followers_count': tweet.author.followers_count,
            'author_friends_count': tweet.author.friends_count,
            'author_created_at': tweet.author.created_at,
            'tweet_id': tweet.id,
            'tweet_created_at': tweet.created_at,
            'tweet_favorite_count': tweet.favorite_count,
            'tweet_retweet_count': tweet.retweet_count,
            'tweet_text': content
        })

    push_tweets(pd.DataFrame(tweet_list))

    return tweet_list
