from rest_framework import serializers
from twitter_analysis_top_trends.models import Hashtag, Tweet


class HashtagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hashtag
        fields = ('id', 'hashtag', 'date')


class TopHashtagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hashtag
        fields = ('id', 'hashtag', 'date')


class TweetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tweet
        fields = ('id', 'author_id', 'author_name', 'author_screen_name', 'author_location', 'author_followers_count',
                  'author_friends_count', 'author_created_at', 'tweet_id', 'tweet_created_at', 'tweet_favorite_count',
                  'tweet_retweet_count', 'tweet_text')
