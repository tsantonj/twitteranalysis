from django.urls import path
from twitter_analysis_top_trends import views

urlpatterns = [
    path('hashtag/', views.HashtagListCreate.as_view()),
    path('tweet/', views.TweetListCreate.as_view()),
    path('top_hashtag/', views.get_top_hashtags, name='top_hashtag'),
    path('search_top_hashtags/', views.search_top_hashtags, name='search_top_hashtags'),
    path('warcraft/', views.search_warcraft, name='warcraft'),
]
