from django.shortcuts import render
from django.http import HttpResponse
from twitter_analysis_top_trends.models import Hashtag, Tweet
from twitter_analysis_top_trends.serializers import HashtagSerializer, TweetSerializer
from rest_framework import generics
import pandas as pd
import json
from twitter_analysis_top_trends import query_twitter


# Create your views here.
class HashtagListCreate(generics.ListCreateAPIView):
    queryset = Hashtag.objects.all()
    serializer_class = HashtagSerializer


class TweetListCreate(generics.ListCreateAPIView):
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer


def get_top_hashtags(request, *args, **kwargs):
    hashtags = Hashtag.objects.all()
    hashtags = pd.DataFrame(list(hashtags.values('hashtag', 'date')))
    hashtags.set_index('date', inplace=True)

    try:
        request = json.loads(request.body.decode())
        since = request.get('since', None)
        until = request.get('until', None)
    except:
        since = request.GET.get('since', None)
        until = request.GET.get('until', None)

    if since and until:
        hashtags = hashtags.loc[str(since):str(until)]

    return HttpResponse(hashtags.hashtag.value_counts().head(20).to_json())


def search_top_hashtags(request, *args, **kwargs):
    # Get request
    try:
        request = json.loads(request.body.decode())
        since = request.get('since', None)
        until = request.get('until', None)
        lang = request.get('lang', "fr")
        min_retweets = request.get('min_retweets', 10)
        n_returns = request.get('n_returns', 1000)
    except:
        since = request.GET.get('since', None)
        until = request.GET.get('until', None)
        lang = request.GET.get('lang', "fr")
        min_retweets = request.GET.get('min_retweets', 10)
        n_returns = request.GET.get('n_returns', 1000)

    # Get secret keys
    API_keys = pd.read_json("./keys.json").to_dict()

    # Authenticate to API
    api = query_twitter.get_twitter_api_access(API_keys)

    # Query API
    hashtags_list = query_twitter.get_random_tweets(API_keys, api, since, until, min_retweets, lang=lang,
                                                  n_returns=n_returns)

    # Convert to pandas.DataFrame
    hashtags = pd.DataFrame(hashtags_list, columns=["hashtag", "date"])

    # Save to the API database
    query_twitter.push_hashtags(hashtags)

    return HttpResponse(hashtags.hashtag.value_counts().head(20).to_json())


def search_warcraft(request, *args, **kwargs):
    try:
        query = json.loads(request.body.decode()).get('query', None)
    except:
        query = request.GET.get('query', None)

    if query:
        # Get secret keys
        API_keys = pd.read_json("./keys.json").to_dict()

        # Authenticate to API
        api = query_twitter.get_twitter_api_access(API_keys)

        # Query API
        query_twitter.get_warcraft_tweets(API_keys, api, query='#warcraft', n_returns=200)

    tweet_list = Tweet.objects.all().order_by('tweet_favorite_count', 'tweet_retweet_count')
    tweet_list = pd.DataFrame(list(tweet_list.values('author_id', 'author_name', 'author_screen_name',
                                                     'author_location', 'author_followers_count',
                                                     'author_friends_count', 'author_created_at', 'tweet_id',
                                                     'tweet_created_at', 'tweet_favorite_count', 'tweet_retweet_count',
                                                  'tweet_text')))

    words = []
    for idx, row in tweet_list.loc[:1000, ['tweet_text']].iterrows():
        words += row.tweet_text.split()

    return HttpResponse(pd.DataFrame(words, columns=['col']).col.value_counts().to_json())
        # json.dumps({
        # 'tweets': tweet_list.loc[:20, ['tweet_text', 'tweet_favorite_count', 'tweet_retweet_count']].to_json(),
        # 'words': pd.DataFrame(words, columns=['col']).col.value_counts().to_json()
    # }))
