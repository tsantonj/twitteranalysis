from django.db import models


# Create your models here.
class Hashtag(models.Model):
    hashtag = models.CharField(max_length=255, blank=False)
    date = models.DateTimeField()


class Tweet(models.Model):
    author_id = models.IntegerField()
    author_name = models.CharField(max_length=255, blank=True)
    author_screen_name = models.CharField(max_length=255, blank=True)
    author_location = models.CharField(max_length=255, blank=True)
    author_followers_count = models.IntegerField(blank=True)
    author_friends_count = models.IntegerField(blank=True)
    author_created_at = models.DateTimeField(blank=True)
    tweet_id = models.IntegerField()
    tweet_created_at = models.DateTimeField(blank=True)
    tweet_favorite_count = models.IntegerField(blank=True)
    tweet_retweet_count = models.IntegerField(blank=True)
    tweet_text = models.CharField(max_length=280)

