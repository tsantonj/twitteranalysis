numpy
pandas~=1.2.4
requests~=2.25.1
django~=3.2.5
djangorestframework~=3.12.4
tweepy~=3.10.0
django-cors-headers
pytz~=2021.1