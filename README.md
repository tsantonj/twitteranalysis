Twitter Analysis <a name="top"/>
===


> Mettre à disposition un Frontend présentant des statistiques de posts Twitter

* Backend:
    - [x] Créer un programme python qui va chercher des posts sur Twitter
    - [x] Classer les posts selon les hashtags et faire ressortir dans un front le top 10 hashtags per hours/days/weeks
    - [x] Vous voulez aller plus loin ? Faites une analyse de sentiments sur le hashtag #warcraft

* Frontend:
    - [x] Exposer une API rest pour un frontend dans React ou Angular qui propose un dashboard de résultats du top ten hashtags
    - [ ] Vous voulez aller plus loin ? Exposez un résultat d'analyse de sentiments

* Bonus:
    - [x] Partagez nous votre code de testing et de validation
    - [ ] Vous avez utilisé une chaine de CICD ? présentez-nous le code associé
    - [ ] Vous avez créé un artifact (docker ou autre) exécutable, associez une documentation d'utilisation et un lien si pertinent
    - [ ] Vous avez développé en local ? Sur un minikube ? Partagez nous vos travaux (Helm, manifests etc etc)

Merci de bien vouloir nous envoyer le liens vers un git avec vos résultats avant Vendredi 23, 18h.


## Table of Contents
- [Installing](#installing)  
- [Followed Steps](#steps) 
    - [Initiate Django](#initback)
    - [Initiate React app](#initui)
    - [API REST](#api)
    - [Back](#backreturn)
    - [Front](#front)
    - [Top hashtags view](#tophashtags)
    - [Dashboard](#dashboard)
    - [Daemon](#daemon)
    - [Warcraft sentiment](#warcraft)
    - [Bonus](#bonus)
- [Conclusion](#conclusion)


## Installing <a name="installing"/>
For this project, I used Python 3.8.8. I coded everything with IntelliJ Idea.

Quick installation of the Python libraries 
```bash
pip install -r requirements.txt
```

Since I know a bit more about React than Angular, I chose to do the front end using the first one.

[Back to top](#top)

---

## Followed Steps <a name="steps"/>
To start with, I checked for a Twitter API. As I've never (or hardly ever) used Twitter before, I tried to create a new junk account, but they need a phone number. So I picked up an old test account. 

The Twitter API looks really easy to use although limited, so I'll try to use it. First of all, I defined the four API tokens in a hidden JSON file. I'm obviously not going to push this to GitHub (it's secret). So yes, I've created a *.gitignore* to prevent pushing the JSON. My job was to get some Tweets and parse them. So, I think I'm going to need to store some Tweets in a database, so I'm taking a look at the info returned by the API. The API retrieves two interesting things for me: Tweets and Trends (source)
[https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api]. 

Then I initiated a *requirements.txt* file, with a few libraries I'm sure to use for a quick setup: 
- Pandas: for data manipulation.
- Numpy: a dependency of Pandas and powerful data processing
- Requests: to make some requests, especially on the Twitter API
- Django: my back will be in Django
- Django REST Framework: for a simple and fast REST API.

Finally, I quickly initiated my front and back end, to have a base. Have a look at the following parts to understand what I did.


[Back to top](#top)

---

### Initiate the Django  <a name="initback"/>
1. Create a Django project `django-admin startproject twitter_analysis .`
2. Create a new module `django-admin startapp twitter_analysis_top_trends`
3. Add the module to *twitter_analysis/settings.py*
```python
INSTALLED_APPS = [
    # omitted for brevity
    'twitter_analysis_top_trends.apps.TwitterAnalysisTopTrendsConfig'
]
```
4. Create a model in *twitter_analysis_top_trends/models.py*
 ```python
from twitter_analysis_top_trends import models

class Tweet(models.Model):
  author = models.CharField(max_length=100, blank=True)
  content = models.CharField(max_length=255, blank=True)
  posted_at = models.DateTimeField()
 ```
5. Do a migration for the new model (to do it quicker, I've create a IntelliJ conf):
```bash
python manage.py makemigrations twitter_analysis_top_trends
python manage.py migrate
```
6. Add REST serializers in *twitter_analysis_top_trends/serializers.py*
```python
from rest_framework import serializers
from twitter_analysis_top_trends.models import Tweet


class TweetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tweet
        fields = ('id', 'author', 'content', 'posted_at')
```
7. Create some views in *twitter_analysis_top_trends/views.py*
```python
from twitter_analysis_top_trends.models import Tweet
from twitter_analysis_top_trends.serializers import TweetSerializer
from rest_framework import generics


# Create your views here (Get and Post).
class TweetListCreate(generics.ListCreateAPIView):
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
```
8. Create *twitter_analysis_top_trends/urls.py*, and add the URLs :
```python
from django.urls import path
from twitter_analysis_top_trends import views

urlpatterns = [
    path('api/tweet/', views.TweetListCreate.as_view()),
]
```
9. Configure the URL mapping in *twitter_analysis/urls.py* :
```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('twitter_analysis_top_trends.urls')),
]
```
10. Enable the rest framework in *twitter_analysis/settings.py* :
```python
INSTALLED_APPS = [
    # omitted for brevity
    'twitter_analysis_top_trends.apps.TwitterAnalysisTopTrendsConfig',
    'rest_framework'
]
```
11. Run the server
```bash
python manage.py runserver
```
> NOTE : The server starts at [localhost:8000](http://localhost:8000/)   
> We have our Tweet database here : [http://127.0.0.1:8000/api/tweet/](http://127.0.0.1:8000/api/tweet/)

Now, we have the base of the back. Next, we'll need to fill the database with some tweets to do some tests !


[Back to top](#top)

---

### Initiate the UI  <a name="initui"/>
For easier and faster beginning, I use *React-App* ([github source](https://github.com/facebook/create-react-app)):
1. Init React-app
```bash
npm init react-app twitter_analysis_front
```
2. Starts the development server with
```bash
npm start
```
> NOTE : The front starts at [localhost:3000](http://localhost:3000/)

Now, we have the base for the front. Next, we'll need to clean the base page and create our front (Dasboards).


[Back to top](#top)

---

### Take a look on the API  <a name="api"/>

After some quick research about Twitter and its API, I have decided to use *Tweepy* and *requests* to do some scraping.
```bash
pip install tweepy
```

I can set my token to the system variables, but I decided to just let them in a JSON file, which I call using Pandas.

Thanks to some well explained tutorial on internet and especially on Twitter API's documentation, the scrapping is quite
easy:
1. API authentication `tweepy.OAuthHandler()`
2. API connection `tweepy.API()`
3. Request the API and create a cursor `tweepy.Cursor()`
4. Walk through the generated cursor

The returned object contains a lot of interesting information that I decided to store in a CSV. But, the text variable which contains the tweet message is limited in length. So, when the message is truncated, I call a Twitter end point to get the given tweet and its full text ([Twitter API documentation](https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets-id)).
To query the end point, I used *requests*.

Finally, with a regex, I extract all # in the tweet and store them in another list and CSV, for easier analyse.

> You can find all this code in the notebook "test_API_Twitter.ipynb".

Now that I know how to get data, I can implement that in the back and store the data in a proper database.


[Back to top](#top)

---

### Back to the back  <a name="backreturn"/>
I change my model to :
````python
from twitter_analysis_top_trends import models

class Hashtag(models.Model):
  hashtag = models.CharField(max_length=255, blank=False)
  date = models.DateTimeField()
````
For the moment, it will be enough.

Now my biggest question is : What is the best way to popularize the database ?
- A daemon which runs in background every hour ? (I think it is the best choice)
- An external script that I call ? (For the moment I will do that through my Jupyter Notebook)
- Via a front call ? (Maybe later)


[Back to top](#top)

---

### Let's create a front  <a name="front"/>
I use some React libraries that I know for the front :  
- "react-router-dom" for an easy rooting
- "reactstrap" for style (bootstrap)
- "axios" to requests the back

First thing to do, is to update package.json and add the default route (to our API) :
```json
{
  "name": "twitter_analysis_front",
  "version": "0.1.0",
  "private": true,
  "proxy": "http://localhost:8000",
  "...": "..."
}
```

Like that, my Axios query will be a little simpler.

Next, I create a structure that I adopt in my previous project:
```raw
twitter_analysis_front/      (Sorry for the name, I don't find an awesome name...)
|-  node_modules/
|-  public/
|-  src/
  |-  assets/                 (A folder for all images, icons, gif...)
  |-  components/             (Here I put all my components. If needed, I can create sub folder, but no need here since 
                              the project is quite small)
  |-  pages/                  (Like for components, but here for the pages)
  |-  style/                  (Quite like before, but for css style sheets. I don't know if I'll create one)
  |-  App.js                  (Here, I will replace the render with the base page structure and specify the routes)
  |-  Index.js                (Here, I just add " import 'bootstrap/dist/css/bootstrap.min.css' " to import it for all 
                              pages and components.)
   • • •
```
This structure is quite clear, simple and enough for a good file organisation.

Next, I'll initiate:
* In pages/ 
  - Index.js, yeah, another one. It's my home page. I'll create dashboards here.
  - HashtagList.js, This page will contain some hashtags in an array
* In components/
  - Header.js is the page header
  - List.js is useful to print my list of hashtag (used by HashtagList.js)
  - WishListLoading.js is a component used when I wait the Axios response

For the moment, I want to do a quick test. So my code is not well formatted. I will come back on that later.

But now, I think about something. I don't want to send all my hashtags list. It's really too long and so, for the front, it will be slow to process it and show the top hashtags. So I need a new end point to query, that returns me just the top hashtag for a specific moment.

Next, I will come back to my front and add 3 arrays:
- top last hour
- top last day
- top last week
And after, I can be also interesting to add a slide bar to select a specific hour / day / week.
  

[Back to top](#top)

---

### Get top hashtags <a name="tophashtags"/>

It's very simple, I check if I have 'since' and 'until' dates as parameters, if so I check the data between these two dates, if not, I take all the data. Then, as in the Jupiter Notebook, I use the values_counts() function to count how many times each term appears. Finally, I send the top 20 in JSON.

```python
# twitter_analysis_top_trends/views.py
from django.http import HttpResponse
from twitter_analysis_top_trends.models import Hashtag
import pandas as pd
import json

# ...

def get_top_hashtags(request, *args, **kwargs):
    hashtags = Hashtag.objects.all()
    hashtags = pd.DataFrame(list(hashtags.values('hashtag', 'date')))
    hashtags.set_index('date', inplace=True)

    request = json.loads(request.body.decode())
    since = request.get('since', None)
    until = request.get('until', None)

    if since and until:
        hashtags = hashtags.loc[str(since):str(until)]

    return HttpResponse(hashtags.hashtag.value_counts().head(20).to_json())
```

I test my request using Insomnia, here an example :
![Request in Insomnia](https://puu.sh/HYf1U/380bf14f93.png)


Finally, I have created a second end point, slower, which will ask for new tweets before sending the response. 
![Request in Insomnia](https://puu.sh/HYg58/e3a0d931ce.png)
But it is slow and, I've got a problem, The API send only recent tweets, even if I pass a specific day in parameters. 
For example, if I ask for tweets of the 8 of july, the API send nearly only tweets between 22h and midnight. I'll search for a better solution later. For the moment, I'll come back on the front.


[Back to top](#top)

---

### Dashboards <a name="dashboard"/>

Create a dashboard in the Index.js with 3 columns :
- top last hour
- top last day
- top last week

The design will be really simple, 3 lists of sort hashtags (from the highest to the lowest).

In addition, I don't want to charge too much for the server and my connection. I have limited data usage and slow bandwidth. So I also created a button to get a batch of new tweets. This is particularly useful for the latest trends.

I've got some errors, which I've correct with these solutions :
- [Fix :  Django CORS Error](https://dzone.com/articles/how-to-fix-django-cors-error)
- [Fix : TypeError: argument of type 'WindowsPath' is not iterable](https://stackoverflow.com/questions/63987965/typeerror-argument-of-type-windowspath-is-not-iterable-in-django-python)

Finally, I have this result :
![first dashboard result](https://puu.sh/HYhHw/e29f01ab21.png)

Oh, and by the way, here the page which lists some hashtags :
![a list page](https://puu.sh/HYhR9/a841b129a3.png)

[Back to top](#top)

---

### Scraping Daemon <a name="daemon"/>


In order to get more data and have a more interesting dashboard, I change the Jupyter Notebook. I created a for loop that will ask for ~300 tweets (2500 / 8) per day. I only ask for one week, because I don't have access to tweets sent more than a week ago. I create an infinite loop that will run the for loop every 15 minutes. In fact, the limit for scraping every 15 minutes is 2500 tweets. 

After a night of scraping (to bypass the 15minutes limit), my dashboard looks like to that:
![](https://puu.sh/HYuqk/6010dd9e91.png)

It looks like perfect ! Now, I will test the bonus questions :
- Warcraft sentiment (sentiment ?)
- CI/CD chain and Docker (I don't know how it works, so I'll try !)


[Back to top](#top)

---

### Warcraft Sentiment <a name="warcraft"/>

**Back**

I have already created the code to get complete tweet, now I just need to create a new model and view. 

Here the model to store a Tweet :
```python
# twitter_analysis_top_trends/models.py
from django.db import models

class Tweet(models.Model):
    author_id = models.IntegerField()
    author_name = models.CharField(max_length=255, blank=True)
    author_screen_name = models.CharField(max_length=255, blank=True)
    author_location = models.CharField(max_length=255, blank=True)
    author_followers_count = models.IntegerField(blank=True)
    author_friends_count = models.IntegerField(blank=True)
    author_created_at = models.DateTimeField(blank=True)
    tweet_id = models.IntegerField()
    tweet_created_at = models.DateTimeField(blank=True)
    tweet_favorite_count = models.IntegerField(blank=True)
    tweet_retweet_count = models.IntegerField(blank=True)
    tweet_text = models.CharField(max_length=280)
```

The view is quite simple, If it receives "query", it will query Twitter API to get new tweets, else, I only look in the
database for the tweets. After, I return the top words, quite like top hashtags before.
```python
# twitter_analysis_top_trends/views.py
from django.http import HttpResponse
from twitter_analysis_top_trends import query_twitter
from twitter_analysis_top_trends.models import Tweet
import pandas as pd
import json


def search_warcraft(request, *args, **kwargs):
    try:
        query = json.loads(request.body.decode()).get('query', None)
    except:
        query = request.GET.get('query', None)

    if query:
        # Get secret keys
        API_keys = pd.read_json("./keys.json").to_dict()

        # Authenticate to API
        api = query_twitter.get_twitter_api_access(API_keys)

        # Query API
        query_twitter.get_warcraft_tweets(API_keys, api, query='#warcraft', n_returns=200)

    tweet_list = Tweet.objects.all().order_by('tweet_favorite_count', 'tweet_retweet_count')
    tweet_list = pd.DataFrame(list(tweet_list.values('author_id', 'author_name', 'author_screen_name',
                                                     'author_location', 'author_followers_count',
                                                     'author_friends_count', 'author_created_at', 'tweet_id',
                                                     'tweet_created_at', 'tweet_favorite_count', 'tweet_retweet_count',
                                                     'tweet_text')))

    words = []
    for idx, row in tweet_list.loc[:1000, ['tweet_text']].iterrows():
        words += row.tweet_text.split()

    return HttpResponse(pd.DataFrame(words, columns=['col']).col.value_counts().to_json())
```
What to do next: 
- Limit to tweets from the last week or day. Otherwise, it is not the current sentiment, but the sentiment of all time (useless).
- Add a hashtag field in the database to allow searching for tweets by hashtag. This may be useful for a possible new query need.



**Front**  

To begin, I create a list of top words (with more than 3 characters), like the top hashtag list before:
![Warcraft sentiment](https://puu.sh/HYvcS/eddaf5fad7.png)
(Oh, and I use this screenshot to show that the front is responsive.)

We can see that a lot of tweets are talking about a reveal today of Warth of the Lich King. After a quick check, a new expansion will be revealed soon. 

With 82 appearing, the word "Thank you" is positive, so the feeling can be good. Let's create some visualizations.

After a quick search, I decide to create a word cloud using WordClouds.js and try a sentiment analysis library from Google. Unfortunately, after 1-2 hours, I couldn't get them to work. I'll try again later if I have some free time on Friday.


> Quick time information, I've taken an average of 15-20 hours on the project so far (I haven't counted well). Knowing that I don't know (e.g. Tweepy, WordCloud, Google sentiment) or don't remember well (Django) how some technologies work, I take a lot of time to try. Let's continue with testing, CI/CD, Artifact Docker and Minikube.



[Back to top](#top)

---

### Testing, CI/CD, Artifact Docker, Minikube <a name="bonus"/>


I want to try to do some testing. I'm not used to writing tests, I know unittest and Jasmine a little bit, but in my previous jobs I was only doing scripts where automated testing is not a priority. I know it's important, but I didn't have time for it before... Let's try it now! 

After that, I will try to build a CI/CD chain. But, I've never done that before, so I'll find out!

Good documentations :
- [testing](https://docs.djangoproject.com/en/3.2/topics/testing/)
- [CI/CD Gitlab](https://gitlab.utc.fr/tsantonj/twitteranalysis/-/ci/editor)



**Test**  

I have created a new folder, twitter_analysis_test/ with a main file test_core.py, which will run all the test codes.
For now, I've created two test files to test the Hashtag and Tweet templates.

Test works :
![console](https://puu.sh/HYxaV/a51e8dfa2a.png)


Now let's try to set up a CI/CD base.

**CI/CD**

Create .gitlab-ci.yml :
```yaml
stages:          # List of stages for jobs, and their order of execution
  - build
  - test
  - deploy

build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "Compiling the code..."
    - npm run twitter_analysis_front/package.json
    - python manage.py check --deploy
    - echo "Compile complete."

unit-test-job:   # This job runs in the test stage.
  stage: test    # It only starts when the job in the build stage completes successfully.
  script:
    - echo "Running model tests..."
    - python manage.py test twitter_analysis_test

deploy-job:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  script:
    - echo "Deploying application..."
    - echo "Application successfully deployed."
```


[Back to top](#top)

---

## Conclusion <a name="conclusion"/>


It took me between 10 and 15 hours to finish the base of the test, and then about 20 hours for a few bonuses.

I didn't manage to do all the bonuses but it was interesting to try. The test part was not too difficult even if I don't remember well how some technologies work. I think it might be interesting to create a pipeline to automate the building, testing, deployment and launch of a project.

Dashboard at 4pm on 7 July 2021, as you can see, it's the opening of the Olympics in Tokyo.
![](https://puu.sh/HYyOb/e5aef50d66.png)


Launch configurations :
* Back:
![](https://puu.sh/HYykT/a550fbeca3.png)
* Front:
![](https://puu.sh/HYykc/6ede0ff8db.png)
* Test :
![](https://puu.sh/HYyjy/a37e3575b6.png)


