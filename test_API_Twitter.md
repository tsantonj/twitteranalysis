Scraping Twitter<a id="top"></a>
===


- Tester Tweepi
- Tester l'API (créer un compte Twitter)
- Tester via `requests`
- Tester le scraping via BSoup si rien n'est OK


sources intéressantes :
- https://developer.twitter.com/en/docs/twitter-api/tweets/sampled-stream/quick-start


```python
import tweepy as tw
import pandas as pd
import numpy as np
import requests
import re
import datetime
import time

# For a progress bar
from ipywidgets import IntProgress
from IPython.display import display
```


```python
# FIXME : To replace with a relatif path
API_keys_path = "D:\Creation_Code\TwitterAnalysisTest\keys.json"
```


```python
# Get keys & tokens
API_keys = pd.read_json(API_keys_path).to_dict()

consumer_key = API_keys["API key"].get(0)
consumer_secret = API_keys["API secret key"].get(0)
access_token = API_keys["access token"].get(0)
access_token_secret = API_keys["access token secret"].get(0)
bearer_token = API_keys["bearer Token"].get(0)
```


```python
def authenticate_to_twitter_API(API_keys:dict) -> tw.api:
    '''
    Connect to Twitter API and return it
    args:
        API_keys : dict of tokens
    returns:
        tw.api
    '''
    
    # Authenticate to Twitter API using Tweepy
    auth = tw.OAuthHandler(API_keys["API key"].get(0), API_keys["API secret key"].get(0))
    auth.set_access_token(API_keys["access token"].get(0), API_keys["access token secret"].get(0))
    
    return tw.API(auth, wait_on_rate_limit=True)

api = authenticate_to_twitter_API(API_keys)
```


```python
def print_tweet(tweet:tw.models.Status):
    '''
    Print tweet content
    '''
    
    print(f"""
author : 
    - name : {tweet.author.name}
    - screen_name @ : {tweet.author.screen_name}
    - location : {tweet.author.location}
    - followers_count : {tweet.author.followers_count} (= followers)
    - friends_count : {tweet.author.friends_count} (= following)
    - created_at : {tweet.author.created_at}
tweet : 
    - coordinates : {tweet.coordinates}
    - geo : {tweet.geo}
    - created_at : {tweet.created_at}
    - favorite_count : {tweet.favorite_count}
    - retweet_count : {tweet.retweet_count}
    - source : {tweet.source}
    - text : 
{tweet.text}
""")
```


```python
def get_tweet(API_keys:dict, tweet_id:int) -> requests.models.Response:
    '''
    Asks to Twitter API for a specific tweet and returns it.
    
    args:
        API_keys : dict of ID tokens
        tweet_id : int represents ID of the tweet
    returns
        requests.models.Response
    '''
    headers = {
        'Authorization': f"Bearer {API_keys['bearer Token'].get(0)}",
    }

    return requests.get(f'https://api.twitter.com/2/tweets/{tweet_id}', headers=headers)
```


```python
def get_hashtag(text:str) -> list:
    '''
    Search for "#" in a text and returns all # and their text.
    
    args:
        text : str
    returns:
        list
    '''
    
    #if "#" in text:
    #    print(re.findall(r'#\w+|@\w+', text))
    #    tag_list = re.findall(r'@\w+', text)
    #    ref_list = re.findall(r'#\w+', text)

    return re.findall(r'#\w+', text) #, tag_list
```


```python
def push_to_API(hashtags:pd.DataFrame, project_API:str="http://127.0.0.1:8000/hashtag/"):
    for idx, row in hashtags.iterrows():
        data = {
            "hashtag": row['hashtag'],
            "date": row['date']
        }

        r = requests.post(project_API, data=data)

        if r.status_code != 201:
            print(r)
```


```python
def get_random_tweets(API_keys:dict, api:tw.api, since:datetime.date, until:datetime.date=None,
                      min_retweets:int=0, lang:str="en", show:bool=False, n_returns:int=5) -> list:
    '''
    Queries Twitter API to get some random tweet.
    
    args:
        API_keys : dict of ID tokens
        api : tw.api
        since : datetime.date represent a date at this format : YYYY-MM-DD
        until : datetime.date represent a date at this format : YYYY-MM-DD
        min_retweets : int, minimum retweet of the tweet (to get most important ones)
        lang : str="en", represents the tweet language
        show : bool=False, set at True if you want to print tweets
        n_returns : int=5, quantity of returned tweets
    returns:
        list of tweets information
    '''
    
    # Progress bar
    f = IntProgress(min=0, max=n_returns)
    display(f) # display the bar
    
    # Query
    search_words = f"since:{since} -filter:replies min_retweets:{min_retweets} lang:{lang} "
    if until:
        search_words += f'until:{until}'
    
    # Collect tweets
    tweets = tw.Cursor(api.search,
                  q=search_words,
                  lang=lang,
                  since=since).items(n_returns)

    tweet_list = []
    hashtags_list = {'hashtag': [], 'date': []}
    
    # Iterate and save and print tweets
    #[tweet.text for tweet in tweets]
    for tweet in tweets:
        # Increment the progress bar
        f.value += 1
        
        # If tweet text is truncated, go search it
        if tweet.truncated:
            # If except is triggered, maybe an authentication error
            try:
                res = get_tweet(API_keys, tweet.id)
                content = res.json().get('data').get('text')
            except:
                # If except is triggered, maybe a connection error or too much demands
                try:
                    print(f'{res.status} {res.title} : "{res.detail}"')
                except:
                    print(res)
                    return tweet_list, hashtags_list
                    
                content = tweet.text
        else:
            content = tweet.text
            
        hash_list = get_hashtag(content)
            
        tweet_list.append({
            'author_id': tweet.author.id,
            'author_name': tweet.author.name,
            'author_screen_name': tweet.author.screen_name,
            'author_location': tweet.author.location,
            'author_followers_count': tweet.author.followers_count,
            'author_friends_count': tweet.author.friends_count,
            'author_created_at': tweet.author.created_at,
            'tweet_id': tweet.id,
            'tweet_coordinates': tweet.coordinates,
            'tweet_created_at': tweet.created_at,
            'tweet_favorite_count' : tweet.favorite_count,
            'tweet_retweet_count' : tweet.retweet_count,
            'tweet_source' : tweet.source,
            'tweet_text' : content,
            'tweet_hashtags': hash_list
        })
        hashtags_list['hashtag'] += hash_list
        hashtags_list['date'] += [tweet.created_at]*len(hash_list)
    
        if show:
            print_tweet(tweet)
            
    return tweet_list, hashtags_list
    
    
try:
    while True:
        j = 0
        
        for i in range(10):
            since = pd.Timestamp.now().date() - pd.Timedelta(i+1, unit='d')
            until = pd.Timestamp.now().date() - pd.Timedelta(i, unit='d')
            lang = 'fr' # en
            min_retweets = j
            n_returns = 200

            tweet_list, hashtags_list = get_random_tweets(API_keys, api, since, until, min_retweets, lang=lang, n_returns=n_returns)

            # Convert to pandas.DataFrame
            hashtags = pd.DataFrame(hashtags_list, columns=["hashtag", "date"])
            tweet_list = pd.DataFrame(tweet_list)

            # Save to CSV
            timestamp = int(pd.Timestamp.timestamp(pd.Timestamp.now()))
            tweet_list.to_csv(f'./Data/twitter/scraping-tweets-{timestamp}.csv', header=True)
            hashtags.to_csv(f'./Data/twitter/scraping-hashtags-{timestamp}.csv', header=True)

            # Save to the API database
            push_to_API(hashtags)
            
            j += 20
            
        # get now tweets
        since = pd.Timestamp.now().date()
        until = pd.Timestamp.now().date() + pd.Timedelta(1, unit='d')
        tweet_list, hashtags_list = get_random_tweets(API_keys, api, since, until, min_retweets, lang=lang, n_returns=n_returns)

        # Convert to pandas.DataFrame
        push_to_API(pd.DataFrame(hashtags_list, columns=["hashtag", "date"]))

        time.sleep(60*15)
except KeyboardInterrupt:
    pass

# Show the top hashtags head
print(hashtags.hashtag.value_counts().head(20))
    
# Show the data head
tweet_list.head()
```


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)


    <Response [429]>
    


    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



    IntProgress(value=0, max=200)



```python

```


```python

```


```python

```


```python

```

[Back to top](#top)

---

Thibault **Santonja**

2021
