import {Table} from "reactstrap";


const ListHashtags = (props) => {
    let hashtags = props.hashtags;
    let column = props.col;
    console.log(typeof hashtags)

    //Logic
    function generateHeader() {
        return (
            <tr key="listHashtag" style={{textAlign: "center"}}>
                <th>Hashtag</th>
                <th>{column}</th>
            </tr>
        );
    }

    function generateList() {
        if (hashtags) {
            return hashtags.map((entry, index) => {
                const {hashtag, date} = entry;

                return (
                    <tr key={index}>
                        <td>{hashtag}</td>
                        <td>{date}</td>
                    </tr>
                )
            })}
    }

    return (
        <Table className="headerFixAlign" bordered>
            <thead>
            {generateHeader()}
            </thead>
            <tbody id="listBody">
            {generateList()}
            </tbody>
        </Table>
    )
}
export {ListHashtags}