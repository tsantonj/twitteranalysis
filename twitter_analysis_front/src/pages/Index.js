import React, { useEffect, useState } from 'react';
import axios from "axios";
import {Button, Col, Container, Row} from "reactstrap";
import WithListLoading from "../components/WithListLoading";
import {ListHashtags} from "../components/List";
import {convertToArray} from "../tools/arrays";


function Index() {
    const [hourState, setHourState] = useState({
        loading: false,
        data: [],
    });
    const [dayState, setDayState] = useState({
        loading: false,
        data: [],
    });
    const [weekState, setWeekState] = useState({
        loading: false,
        data: [],
    });

    // Effect
    useEffect(() => {
        let today = new Date();


        setWeekState({ loading: true });
        fetchData('/top_hashtag/',
            new Date(today.getFullYear(), today.getMonth(), today.getDate()-7),
            new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours()+1),
            setWeekState)

        setDayState({ loading: true });
        fetchData('/top_hashtag/',
            new Date(today.getFullYear(), today.getMonth(), today.getDate()-1, today.getHours()),
            new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours()+1),
            setDayState)

        setHourState({ loading: true });
        fetchData('/top_hashtag/',
            new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours()-2),
            new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours()+1),
            setHourState)
    }, []);


    // Data
    function fetchData(url, since, until, stateFunction) {
        console.log('query ' + url + ', since ' + since + ', until ' + until)
        axios
            .get(url, {params: {
                since: since.toISOString().substring(0, 19),
                until: until.toISOString().substring(0, 19)}})
            .then((res)=>{
                console.log(res.data);
                stateFunction({ loading: false, data: res.data});
            })
            .catch(error => {
                console.error(error);
            });
    }


    return(
        <>
            <Container>
                <Row>
                    <h1>Hashtag trends on Twitter</h1>
                </Row>
                <br/>
                <Row>
                    <Col>
                        <h2>Top hour :</h2>
                        {hourState.data !== null ? (
                            <ListHashtags hashtags={convertToArray(hourState.data)} col={'Count'} />
                        ) : (
                            <WithListLoading />
                        )}
                    </Col>
                    <Col>
                        <h2>Top day :</h2>
                        {dayState.data !== null ? (
                            <ListHashtags hashtags={convertToArray(dayState.data)} col={'Count'} />
                        ) : (
                            <WithListLoading />
                        )}
                    </Col>
                    <Col>
                        <h2>Top week :</h2>
                        {weekState.data !== null ? (
                            <ListHashtags hashtags={convertToArray(weekState.data)} col={'Count'} />
                        ) : (
                            <WithListLoading />
                        )}
                    </Col>
                </Row>
                <Row>
                    <Button color="primary" onClick={() => axios.get('/search_top_hashtags/').then().catch()}>Make a request (need some minutes)</Button>
                </Row>
            </Container>
        </>
    );
}

export default Index;
