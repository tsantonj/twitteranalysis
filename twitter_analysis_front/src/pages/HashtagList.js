import React, { useEffect, useState } from 'react';
import axios from "axios";
import {Col, Container, Row} from "reactstrap";
import {ListHashtags} from "../components/List";
import WithListLoading from "../components/WithListLoading";

function HashtagList() {
    const [appState, setAppState] = useState({
        loading: false,
        data: [],
    });



    // Effect
    useEffect(() => {
        setAppState({ loading: true });

        fetchData();
    }, []);


    // Data
    function fetchData() {
        axios
            .get('/hashtag/')
            .then((res)=>{
                let i = Math.floor(Math.random() * (res.data.length-20))
                console.log(res.data.slice(i, i+10));
                setAppState({ loading: false, data: res.data.slice(i, i+10) });
            })
            .catch(error => {
                console.error(error);
            });
    }


    return(
        <>
            <Container>
                <Row>
                    <h1>10 random hashtags :</h1>
                </Row>
                <Row>
                    <Col>
                        {appState.data !== null ? (
                            <ListHashtags hashtags={appState.data} col={'Date'} />
                        ) : (
                            <WithListLoading />
                        )}
                    </Col>
                </Row>
            </Container>
        </>
    );
}
export default HashtagList;
