import React, { useEffect, useState } from 'react';
import {Col, Row} from "reactstrap";
import axios from "axios";
import WithListLoading from "../components/WithListLoading";
import {ListHashtags} from "../components/List";
import {convertToArray} from "../tools/arrays";


function HashtagList() {
    const [appState, setAppState] = useState({
        loading: false,
        data: [],
    });


    // Effect
    useEffect(() => {
        setAppState({ loading: true });

        fetchData();
    }, []);


    // Data
    function fetchData() {
        axios
            .get('/warcraft/')
            .then((res)=>{
                setAppState({ loading: false, data: res.data});
            })
            .catch(error => {
                console.error(error);
            });
    }

    function clearLittleWords(list, limit) {
        return list.filter(function(obj){
            return obj.hashtag.length > limit;
        })
    }


    return(
        <>
            <Row>
                <h1>Warcraft sentiment</h1>
            </Row>
            <br/>
            <Row>
                <Col>
                    <h2>Top words</h2>
                    {appState.data !== null ? (
                        <ListHashtags hashtags={clearLittleWords(convertToArray(appState.data), 3)} col={'Count'} />
                    ) : (
                        <WithListLoading />
                    )}
                </Col>
                <Col>
                    <Row>
                        <h2>Word cloud</h2>
                        <p>Todo</p>
                    </Row>
                    <Row>
                        <h2>Sentiment analysis</h2>
                        <p>Todo</p>
                    </Row>
                </Col>
            </Row>
        </>
    );
}

export default HashtagList;
