import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {useEffect} from 'react';
import Index from "./pages/Index";
import {Header} from "./components/Header";
import HashtagList from "./pages/HashtagList";
import Sentiment from "./pages/Sentiment";

function App() {
    useEffect(() => {
        document.title = "Twitter Trends"
    }, []);

  return (
    <div className="App">
        <Header />
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Index}/>
                <Route exact path="/list" component={HashtagList}/>
                <Route exact path="/warcraft" component={Sentiment}/>
            </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
