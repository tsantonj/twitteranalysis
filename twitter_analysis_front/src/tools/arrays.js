
function convertToArray(result) {
    try {
        return Object.entries(result).map(([hashtag, date]) => ({hashtag, date}))
    } catch (error) {
        return []
    }
}

export {convertToArray}