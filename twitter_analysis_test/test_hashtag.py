from django.test import TestCase
from twitter_analysis_top_trends.models import Hashtag
import pandas as pd
from django.utils import timezone
import pytz


timezone.now()


class HashtagTestCase(TestCase):
    def setUp(self):
        Hashtag.objects.create(hashtag="#JOTokyo2021", date=pd.Timestamp.now(tz=pytz.UTC))

    def test_hashtag(self):
        """Animals that can speak are correctly identified"""
        jo_tokyo = Hashtag.objects.get(hashtag="#JOTokyo2021")
