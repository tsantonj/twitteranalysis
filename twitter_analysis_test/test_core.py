from django.test import TestCase
from twitter_analysis_test import test_hashtag, test_tweet


class CoreTestCase(TestCase):
    def setUp(self):
        test_hashtag.HashtagTestCase().setUp()
        test_tweet.TweetTestCase().setUp()
