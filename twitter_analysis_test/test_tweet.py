from django.test import TestCase
from twitter_analysis_top_trends.models import Tweet
import pandas as pd
from django.utils import timezone
import pytz


timezone.now()


class TweetTestCase(TestCase):
    def setUp(self):
        Tweet.objects.create(
            author_id=2021,
            author_name='Japan',
            author_screen_name='Japan',
            author_location='Earth',
            author_followers_count='7000000000',
            author_friends_count='125000000',
            author_created_at=pd.Timestamp.now(tz=pytz.UTC),
            tweet_id=20210723,
            tweet_created_at=pd.Timestamp.now(tz=pytz.UTC),
            tweet_favorite_count='654654654',
            tweet_retweet_count='76546546546',
            tweet_text="JO's starting !!"
        )

    def test_hashtag(self):
        """Animals that can speak are correctly identified"""
        jo_tokyo = Tweet.objects.get(tweet_id=20210723)
